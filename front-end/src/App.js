import React, { Component } from 'react'
import { DataTable } from './wwd/wwd'
import * as backend from './services/backend.service'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'

class App extends Component {

  state = {
    nodes: [
      { name: 'Austin' },
      { name: 'Cooper' },
      { name: 'Steve' },
      { name: 'Will' }
    ],
    edges: [],
    columns: [
      { key: 'name', renderHead: () => null },
      {
        render: (_, r, c, row) => this.renderCell(c-1, row.id),
        renderHead: () => 'Austin'
      },
      {
        render: (_, r, c, row) => this.renderCell(c-1, row.id),
        renderHead: () => 'Cooper'
      },
      {
        render: (_, r, c, row) => this.renderCell(c-1, row.id),
        renderHead: () => 'Steve'
      },
      {
        render: (_, r, c, row) => this.renderCell(c-1, row.id),
        renderHead: () => 'Will'
      }
    ]
  }

  componentDidMount() {
    backend.fetchNodes((err, nodes) => {
      if (err) throw err
      this.setState({ nodes })
    })
    backend.fetchEdges((err, edges) => {
      if (err) throw err
      this.setState({ edges })
    })
  }

  renderCell(to, from) {
    const edge = this.state.edges.filter(e => e.from === from && e.to === to)[0]
    return edge ? edge.amount : null
  }
  render() {
    return (
      <div className='container'>
        <DataTable data={this.state.nodes}
                   columns={this.state.columns}
                   sortable={false}
                   searchable={false}
                   pagination={false}
                   classes={{ table: 'table-bordered' }} />
      </div>
    )
  }
}

export default App;
