export const toReadable = str => (str.match(/[A-Za-z][a-z]*/g) || [])
  .map(word => word[0].toUpperCase() + word.substring(1))
  .join(' ')
export const isUuid = str => (str.match(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i))
export const strCompare = (lhs, rhs) => lhs < rhs ? -1 : lhs > rhs
export const classList = (...classes) => {
  return classes
    .filter(clazz => clazz)
    .filter((clazz, i, list) => list.indexOf(clazz) === i)
    .join(' ')
}