export const defaultComparator = (lhs, rhs) => {
  if (typeof lhs === 'number' && typeof rhs === 'number') return lhs - rhs
  if (typeof lhs === 'boolean' && typeof rhs === 'boolean') return lhs === rhs ? 0 : lhs ? -1 : 1
  if (lhs === undefined || rhs === undefined) return rhs === undefined
  lhs = lhs.toString()
  rhs = rhs.toString()
  return lhs < rhs ? -1 : lhs > rhs
}

export const mergeSort = arr => {
  const len = arr.length
  if (len < 2) return arr
  const mid = Math.floor(len / 2)
  const left = arr.slice(0, mid)
  const right = arr.slice(mid)
  return merge(mergeSort(left), mergeSort(right))
}
const merge = (lhs, rhs) => {
  const result = []
  const lLen = lhs.length, rLen = rhs.length
  let l = 0, r = 0
  while (l < lLen && r < rLen)
    lhs[l] < rhs[r]
      ? result.push(lhs[l++])
      : result.push(rhs[r++])
  return result.concat(lhs.slice(l)).concat(rhs.slice(r))
}

export const filterInPlace = (arr, condition) => {
  let j = 0

  for (let i = 0; i < arr.length; ++i) {
    const el = arr[i]
    if (condition(el, i, arr)) arr[j++] = el
  }

  arr.length = j
  return arr
}

export const keys = arr => {
  return Object.keys(arr.reduce((lhs, rhs) => ({ ...lhs, ...rhs }), {}))
}