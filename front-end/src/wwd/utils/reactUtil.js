export const innerText = reactElement => {
  const text = [reactElement]
  while (!text.every(el => typeof el !== 'object'))
    for (let i = 0; i < text.length; ++i) {
      if (typeof text[i] !== 'object') continue
      const { children } = text[i].props
      if (children !== undefined)
        text.push(...(children[0] === undefined ? [children] : children))
      text[i] = ''
    }
  return text.reduce((prev, col) => prev + col.toString(), '')
}