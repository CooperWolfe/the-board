import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  ButtonGroup,
  Button
} from 'reactstrap'
import { classList } from '../../utils/stringUtil'

const propTypes = {
  // Input
  numPages: PropTypes.number.isRequired,
  defaultPage: PropTypes.number,
  className: PropTypes.string,
  style: PropTypes.object,
  // Events
  onChange: PropTypes.func
}
const defaultProps = {
  // Input
  defaultPage: 1,
  className: '',
  style: {},
  // Events
  onChange() {}
}

/**
 * <h2>Input</h2>
 *   numPages (required): number
 *   defaultPage: number
 *   className: string
 *   style: object
 * <h2>Events</h2>
 *   onChange: (page: number) => any
 */
class PaginationButtons extends Component {

  // Mount
  state = {
    page: this.props.defaultPage
  }

  // Derive
  static getPage(props, state) {
    return state.page > props.numPages ? props.numPages
         : state.page < 1 ? 1 : state.page
  }
  static getDerivedStateFromProps(props, state) {
    const page = PaginationButtons.getPage(props, state)
    return { ...state, page }
  }

  // Render
  getPages() {
    const { numPages } = this.props

    const pages = Array(numPages)
    for (let i = 0; i < numPages; ++i)
      pages[i] = i + 1
    return pages
  }
  renderEnoughButtons() {
    const { page } = this.state
    const pages = this.getPages()

    return pages.map(curr => (
      <Button key={curr}
              color={curr === page ? 'secondary' : 'outline-secondary'}
              onClick={() => this.handlePageChange(curr)}
              onKeyUp={e => this.handleKeyUp(e.which)}>
        { curr }
      </Button>
    ))
  }
  renderOverflowButtons() {
    const { page } = this.state
    const { numPages } = this.props

    return (
      <React.Fragment>
        <Button color={ page === 1 ? 'secondary' : 'outline-secondary' }
                onClick={() => this.handlePageChange(1)}
                onKeyUp={e => this.handleKeyUp(e.which)}>1</Button>
        <Button color={ page === 2 ? 'secondary' : 'outline-secondary' }
                onClick={() => this.handlePageChange(2)}
                disabled={page > 3}
                onKeyUp={e => this.handleKeyUp(e.which)}>
          { page <= 3 ? 2 : '...' }
        </Button>
        <Button color={ page >= 3 && page <= numPages - 2 ? 'secondary' : 'outline-secondary' }
                onClick={() => page < 3 ? this.handlePageChange(3)
                             : page > numPages - 2 ? this.handlePageChange(numPages - 2)
                             : null}
                onKeyUp={e => this.handleKeyUp(e.which)}>
          { page <= 3 ? 3 : page >= numPages - 2 ? numPages - 2 : page }
        </Button>
        <Button color={ page === numPages - 1 ? 'secondary' : 'outline-secondary' }
                onClick={() => this.handlePageChange(numPages - 1)}
                disabled={page < numPages - 2}
                onKeyUp={e => this.handleKeyUp(e.which)}>
          { page >= numPages - 2 ? numPages - 1 : '...' }
        </Button>
        <Button color={ page === numPages ? 'secondary' : 'outline-secondary' }
                onClick={() => this.handlePageChange(numPages)}
                onKeyUp={e => this.handleKeyUp(e.which)}>
          { numPages }
        </Button>
      </React.Fragment>
    )
  }
  render() {
    const { page } = this.state
    const { className, style, numPages } = this.props

    return (numPages <= 0 || isNaN(numPages)) || (
      <ButtonGroup className={classList(className)} style={style}>
        <Button color='outline-secondary'
                onClick={() => this.handlePageChange(page - 1)}
                disabled={page === 1}>
          <i className='fa fa-caret-left'/>
        </Button>
        { numPages <= 5 ? this.renderEnoughButtons() : this.renderOverflowButtons() }
        <Button color='outline-secondary'
                onClick={() => this.handlePageChange(page + 1)}
                disabled={page === numPages}>
          <i className='fa fa-caret-right'/>
        </Button>
      </ButtonGroup>
    )
  }

  // Events
  handlePageChange = page => {
    const { onChange, numPages } = this.props
    if (page < 1 || page > numPages) return
    this.setState({ ...this.state, page })
    onChange(page)
  }
  handleKeyUp = keyCode => {
    switch (keyCode) {
    case 37: return this.handlePageChange(this.state.page - 1) // left
    case 38: return this.handlePageChange(this.props.numPages) // up
    case 39: return this.handlePageChange(this.state.page + 1) // right
    case 40: return this.handlePageChange(1)                   // down
    default: return
    }
  }
}

PaginationButtons.propTypes = propTypes
PaginationButtons.defaultProps = defaultProps

export default PaginationButtons