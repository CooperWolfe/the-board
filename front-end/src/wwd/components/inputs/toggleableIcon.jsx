import React from 'react'
import PropTypes from 'prop-types'
import { classList } from '../../utils/stringUtil'

const propTypes = {
  // Input
  name: PropTypes.string.isRequired,
  isFilled: PropTypes.bool.isRequired,
  className: PropTypes.string,
  style: PropTypes.object,
  // Events
  onClick: PropTypes.func
}
const defaultProps = {
  className: '',
  style: {},
  onClick() {}
}

/**
 * <h2>Input</h2>
 *   name (required): string
 *   isFilled (required): boolean
 *   className: string
 *   style: object
 * <h2>Events</h2>
 *   onClick(event: any) => any
 */
const ToggleableIcon = props => {
  let { className } = props
  const { name, isFilled, onClick, style } = props
  const icon = `fa-${name}${isFilled ? '' : '-o'}`
  className = classList('fa', icon, className)

  return (
    <i className={className} onClick={onClick} style={style} />
  )
}

ToggleableIcon.propTypes = propTypes
ToggleableIcon.defaultProps = defaultProps

export default ToggleableIcon