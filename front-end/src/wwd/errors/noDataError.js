class NoDataError extends Error {
  static status = 0x18600442
  status = NoDataError.status
  message = 'There is no data to display.'

  constructor(message, status) {
    super()
    if (message !== undefined) this.message = message
    if (status !== undefined) this.status = status
  }
}

export default NoDataError