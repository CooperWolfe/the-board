const http = require('axios')

const addr = process.env.REACT_APP_BACKEND_ADDR
const port = process.env.REACT_APP_BACKEND_PORT

if (!addr) throw new Error('Environment variable BACKEND_ADDR must be set')
if (!port) throw new Error('Environment variable BACKEND_PORT must be set')

/**
 * generates the full url given a route
 * @param { string } route
 * @return string
 */
function src(route) {
  if (route[0] !== '/') route = `/${route}`
  return `http://${addr}:${port}${route}`
}

/**
 * fetches nodes from backend
 * @param { function } cb (err: any, nodes: any[]) => any
 * @return void
 */
export function fetchNodes(cb) {
  http.get(src('/nodes'))
    .then(res => {
      cb(null, res.data)
    })
    .catch(err => {
      cb(err)
    })
}

/**
 * fetches edges from backend
 * @param { function } cb (err: any, edges: any[]) => any
 * @return void
 */
export function fetchEdges(cb) {
  http.get(src('/edges'))
    .then(res => {
      cb(null, res.data)
    })
    .catch(err => {
      cb(err)
    })
}
