const router = require('express').Router({})
const dbService = require('../services/db.service')

router.get('/nodes', (req, res, next) => {
  dbService.fetchNodes((err, nodes) => {
    if (err) return next(err)
    res.send(nodes)
  })
})
router.get('/edges', (req, res, next) => {
  dbService.fetchEdges((err, edges) => {
    if (err) return next(err)
    res.send(edges)
  })
})

module.exports = router
