const { GraphDatabase } = require('neo4j')

const user = process.env.DATABASE_USER
const pass = process.env.DATABASE_PASS
const addr = process.env.DATABASE_ADDR
const port = process.env.DATABASE_PORT

if (!user) throw 'Environment variable DATABASE_USER must be set'
if (!pass) throw 'Environment variable DATABASE_PASS must be set'
if (!addr) throw 'Environment variable DATABASE_ADDR must be set'
if (!port) throw 'Environment variable DATABASE_PORT must be set'

const db = new GraphDatabase(`http://${ user }:${ pass }@${ addr }:${ port }`)

fetchNodes((err, res) => {
  if (err) throw err
  !res.length && initialize(err => {
    if (err) throw err
  })
})

/**
 * Initializes database
 * @param { function } cb (err, result) => any
 * @return void
 */
function initialize(cb) {
  db.cypher(`
    CREATE (Austin:Board {name: 'Austin'})
    CREATE (Cooper:Board {name: 'Cooper'})
    CREATE (Steve:Board {name: 'Steve'})
    CREATE (Will:Board {name: 'Will'})
    CREATE 
      (Austin)-[:OWES {amount: 0}]->(Cooper),
      (Austin)-[:OWES {amount: 0}]->(Steve),
      (Austin)-[:OWES {amount: 0}]->(Will),
      (Cooper)-[:OWES {amount: 0}]->(Austin),
      (Cooper)-[:OWES {amount: 0}]->(Steve),
      (Cooper)-[:OWES {amount: 0}]->(Will),
      (Steve)-[:OWES {amount: 0}]->(Austin),
      (Steve)-[:OWES {amount: 0}]->(Cooper),
      (Steve)-[:OWES {amount: 0}]->(Will),
      (Will)-[:OWES {amount: 0}]->(Austin),
      (Will)-[:OWES {amount: 0}]->(Cooper),
      (Will)-[:OWES {amount: 0}]->(Steve)
    `, cb)
}

/**
 * Fetches nodes from neo4j database
 * @param { function } cb (err: any, nodes: any[]) => any
 */
function fetchNodes(cb) {
  db.cypher(`
    MATCH (n:Board) RETURN n
  `, (err, res) => {
    if (err) return cb(err)
    cb(null, res.map(el => ({ id: el.n._id, ...el.n.properties })))
  })
}

/**
 * Fetches edges from neo4j database
 * @param { function } cb (err: any, edges: any[]) => any
 */
function fetchEdges(cb) {
  db.cypher(`
    MATCH (:Board)-[e:OWES]->(:Board) RETURN e
  `, (err, res) => {
    if (err) return cb(err)
    cb(null, res.map(el => ({ from: el.e._fromId, to: el.e._toId, ...el.e.properties })))
  })
}

module.exports = {
  fetchNodes,
  fetchEdges
}
